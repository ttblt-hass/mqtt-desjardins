"""Module defining entrypoint."""
import asyncio

from mqtt_desjardins import device


def main():
    """Entrypoint function."""
    dev = device.MqttDesjardins()
    asyncio.run(dev.async_run())


if __name__ == "__main__":
    main()
