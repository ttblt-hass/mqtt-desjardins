"""Mqtt desjardins module."""
import asyncio
import json
import os
import time

import yaml
import mqtt_hass_base
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import InvalidSessionIdException, NoSuchElementException
from selenium import webdriver
from selenium.webdriver.common.by import By


class MqttDesjardins(mqtt_hass_base.MqttDevice):
    """MQTT Desjardins."""

    def __init__(self):
        """Constructor."""
        mqtt_hass_base.MqttDevice.__init__(self, "mqtt-desjardins")
        self.driver = None

    def read_config(self):
        """Read env vars."""
        self.main_loop_wait_time = os.environ.get("MAIN_LOOP_WAIT_TIME", 900)
        self.hub_url = os.environ.get("HUB_URL", "http://127.0.0.1:4444/wd/hub")
        self.config_file = os.environ.get("CONFIG_FILE", "/etc/mqtt_desjardins/settings.yaml")
        with open(self.config_file) as fhc:
            self.config = yaml.load(fhc, Loader=yaml.FullLoader)

    def _on_connect(self, client, userdata, flags, rc):
        """MQTT on connect callback."""

    def _on_publish(self, client, userdata, mid):
        """MQTT on publish callback."""

    def _mqtt_subscribe(self, client, userdata, flags, rc):
        """Subscribe to all needed MQTT topic."""

    def _on_message(self, client, userdata, msg):
        """MQTT on message callback."""

    def _signal_handler(self, signal_, frame):
        """Handle SIGKILL."""

    async def _init_main_loop(self):
        """Init before starting main loop."""

    async def _main_loop(self):  # pylint: disable=R0914
        """Run main loop."""
        self.logger.debug("Fetching data")
        self.driver = webdriver.Remote(
           command_executor=self.hub_url,
           desired_capabilities=DesiredCapabilities.FIREFOX)
        wait = WebDriverWait(self.driver, 10)
        # Go the home page
        self.logger.debug("Login page")
        self.driver.get("https://accweb.mouv.desjardins.com/identifiantunique/identification")
        # Fill the form
        elem = self.driver.find_element_by_id("codeUtilisateur")
        elem.click()
        for key in self.config['number']:
            elem.send_keys(key)
            time.sleep(0.1)
        elem.click()
        # Validate the form
        elem = self.driver.find_element_by_xpath("//input[@value='Entrer']")
        elem.click()
        # Defi page
        if self.driver.title != "S'authentifier | Desjardins":
            wait.until(EC.presence_of_element_located((By.ID, "valeurReponse")))

            question_found = False
            for qanda in self.config['questions']:
                question = qanda['question']
                answer = qanda['answer']
                try:
                    question_xpath = "//b[contains(.,'{}')]".format(question)
                    elem = self.driver.find_element_by_xpath(question_xpath)
                except NoSuchElementException:
                    continue
                elem = self.driver.find_element_by_id("valeurReponse")
                elem.send_keys(answer)
                elem = self.driver.find_element_by_id("conserverNon")
                elem.click()
                question_found = True
                break

            if not question_found:
                await self._wait_for_next_run()
                return

            elem.submit()

        # Login page
        wait.until(EC.presence_of_element_located((By.ID, "motDePasse")))

        elem = self.driver.find_element_by_xpath("//strong[@class='authentificationPhrase']")
        if not elem:
            self.logger.error("Authentification phrase not found.")
            await self._wait_for_next_run()
            return
        if elem.text != self.config['secure_phrase']:
            self.logger.error("Authentification phrase not valid.")
            await self._wait_for_next_run()
            return

        self.logger.debug("Password page")
        elem = self.driver.find_element_by_id("motDePasse")
        elem.click()
        elem.send_keys(self.config['password'])
        elem.click()
        elem.submit()

        # Wait for page loading
        wait.until(EC.presence_of_element_located((By.ID, "btnMessages")))
        wait.until(EC.presence_of_element_located((By.ID, "collapseEpargnePlacementMsg")))

        elem = self.driver.find_element_by_id("collapseEpargnePlacementMsg")
        elem.click()

        # It's "compte " with a space
        for account_type in ('compte ', 'financement', 'epargne'):
            elements = self.driver.find_elements_by_xpath("//div[@class='produit "
                                                          "{}']".format(account_type))

            self.logger.debug("%s", account_type)
            for elem in elements:
                amount_el = elem.find_element_by_xpath(".//span[@class='montant']")
                amount, devise = amount_el.text.rsplit(" ", 1)
                amount = amount.replace(",", ".").replace(" ", "")
                amount = float(amount)

                name_el = elem.find_element_by_xpath("div[1]//h3")
                account_id, account_name = name_el.text.split(" ", 1)

                description_el = elem.find_element_by_xpath("div[1]//p")
                comment = None
                caisse = description_el.find_elements_by_xpath(".//span")[0].text
                caisse = caisse.strip()
                if len(description_el.find_elements_by_xpath(".//span")) > 1:
                    comment = description_el.find_elements_by_xpath(".//span")[1].text
                    comment = comment.strip(" -")

                mqtt_base_topic = "{}/sensor/{}/{}".format(self.mqtt_root_topic,
                                                           self.name,
                                                           account_id.lower(),)

                mqtt_config_topic = mqtt_base_topic + "/config"
                mqtt_state_topic = mqtt_base_topic + "/state"

                sensor_config = {
                        "name": account_id,
                        "state_topic": mqtt_state_topic,
                        "unit_of_measurement": devise,
                        "value_template": "{{ value_json.account.value }}",
                        "json_attributes_topic": mqtt_state_topic,
                        "expire_after": self.main_loop_wait_time * 5,
                        "json_attributes_template": "{{ value_json.account | tojson }}",
                        "unique_id": account_id,
                        "device": {"identifiers": self.name}
                }
                self.logger.debug("%s - %s: %s %s", account_id, account_name, amount, devise)
                self.logger.debug("New sensor config: %s, %s", mqtt_config_topic, sensor_config)
                self.mqtt_client.publish(topic=mqtt_config_topic,
                                         retain=True,
                                         payload=json.dumps(sensor_config))

                sensor_state = {
                    "account": {"value": amount,
                                "caisse": caisse,
                                "account_id": account_id,
                                "name": account_name,
                                }
                }
                if comment:
                    sensor_state["account"]["comment"] = comment

                self.logger.debug("Send sensor data: %s - %s", mqtt_state_topic, sensor_state)
                self.mqtt_client.publish(topic=mqtt_state_topic,
                                         payload=json.dumps(sensor_state))
        self.driver.delete_all_cookies()
        self.driver.close()

        await self._wait_for_next_run()

    async def _wait_for_next_run(self):
        """Wait for the next run."""
        i = 0
        while i < self.main_loop_wait_time and self.must_run:
            await asyncio.sleep(1)
            i += 1

    async def _loop_stopped(self):
        """Run after the end of the main loop."""
        try:
            self.driver.close()
        except InvalidSessionIdException:
            pass
